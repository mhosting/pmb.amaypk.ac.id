require 'test_helper'

class FormsControllerTest < ActionController::TestCase
  setup do
    @form = forms(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:forms)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create form" do
    assert_difference('Form.count') do
      post :create, form: { agama: @form.agama, alamat_ortu: @form.alamat_ortu, alamat_sekolah: @form.alamat_sekolah, asal_sekolah: @form.asal_sekolah, bb: @form.bb, email: @form.email, email_sekolah: @form.email_sekolah, jk: @form.jk, job_ortu: @form.job_ortu, jurusan: @form.jurusan, nama_gbk: @form.nama_gbk, nama_ortu: @form.nama_ortu, name: @form.name, penghasilan_ortu: @form.penghasilan_ortu, prodi: @form.prodi, reg: @form.reg, tb: @form.tb, telp_ortu: @form.telp_ortu, tlp_bk: @form.tlp_bk, tlp_hp: @form.tlp_hp, ttl: @form.ttl, web_sekolah: @form.web_sekolah }
    end

    assert_redirected_to form_path(assigns(:form))
  end

  test "should show form" do
    get :show, id: @form
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @form
    assert_response :success
  end

  test "should update form" do
    put :update, id: @form, form: { agama: @form.agama, alamat_ortu: @form.alamat_ortu, alamat_sekolah: @form.alamat_sekolah, asal_sekolah: @form.asal_sekolah, bb: @form.bb, email: @form.email, email_sekolah: @form.email_sekolah, jk: @form.jk, job_ortu: @form.job_ortu, jurusan: @form.jurusan, nama_gbk: @form.nama_gbk, nama_ortu: @form.nama_ortu, name: @form.name, penghasilan_ortu: @form.penghasilan_ortu, prodi: @form.prodi, reg: @form.reg, tb: @form.tb, telp_ortu: @form.telp_ortu, tlp_bk: @form.tlp_bk, tlp_hp: @form.tlp_hp, ttl: @form.ttl, web_sekolah: @form.web_sekolah }
    assert_redirected_to form_path(assigns(:form))
  end

  test "should destroy form" do
    assert_difference('Form.count', -1) do
      delete :destroy, id: @form
    end

    assert_redirected_to forms_path
  end
end
