class Welcome < ActionMailer::Base
  
  default from: "Panitia MABA AMA YPK #{Setting.first.ta}<ama.ypk@gmail.com>"

  def send_reply(mail)
    #attachments["rails.png"] = File.read("#{Rails.root}/public/assets/rails.png")
    mail(:to => mail, :subject => "Pendaftaran Online")
  end
  
  def surat_balasan(form)
    @form = form
    #attachments["keterangan.pdf"] = File.read("#{Rails.root}/public/documents/keterangan.pdf")
    mail(:to => @form.email, :subject => "[Pendaftaran:#{@form.reg}] Balasan Pendaftaran Online")
  end
  
  def send_data(form, mail)
    @form = form
    mail(:to => mail, :subject => "Data Pendaftaran Online")
  end
  
end
