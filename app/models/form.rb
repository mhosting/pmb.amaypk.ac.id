class Form < ActiveRecord::Base
  attr_accessible :alamat, :agama, :alamat_ortu, :alamat_sekolah, :asal_sekolah, :bb, :email, :email_sekolah, :jk, :job_ortu, :jurusan, :nama_gbk, :nama_ortu, :name, :penghasilan_ortu, :prodi, :reg, :tb, :telp_ortu, :tlp_bk, :tlp_hp, :ttl, :web_sekolah,
    :alamat_pribadi_dusun, :alamat_pribadi_rt, :alamat_pribadi_kecamatan, :alamat_pribadi_kelurahan_desa, :alamat_pribadi_kabupaten, :alamat_pribadi_propinsi, :alamat_sekolah_dusun, :alamat_sekolah_rt, :alamat_sekolah_kecamatan, :alamat_sekolah_kelurahan_desa, :alamat_sekolah_kabupaten, :alamat_sekolah_propinsi, :alamat_ortu_dusun, :alamat_ortu_rt, :alamat_ortu_kecamatan, :alamat_ortu_kelurahan_desa, :alamat_ortu_kabupaten, :alamat_ortu_propinsi
  scope :by_year, lambda { |year| where('extract(year from created_at) = ?', year) }
  validates_uniqueness_of :email
  validates :name, presence: true
end
