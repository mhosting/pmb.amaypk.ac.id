class FormsController < ApplicationController
  #before_filter :authenticate_user!
  before_filter :set_form, only: [:show, :edit, :update, :destroy]
  require 'active_support/core_ext'

  respond_to :html

  def index
    @forms = Form.all
    respond_with(@forms)
  end

  def show
    respond_with(@form)
  end

  def new
    @year = Time.now.strftime("%Y").to_i
    @month = Time.now.strftime("%m").to_i
    @date = Time.now.strftime("%d").to_i
    @form = Form.new
    @reg = Form.all.size + 333
    respond_with(@form)
  end

  def edit
  end

  def create
    registrar = Form.all.size + 333
    year = Time.now.strftime("%Y").to_i
    month = Time.now.strftime("%m").to_i
    date = Time.now.strftime("%d").to_i
    @form = Form.new(params[:form])
    @form.reg = "#{"%04d" % registrar}/#{Setting.first.ta}"
    @form.ta = "#{Setting.first.ta}"
    @form.gelombang = gelombang(Date.new(year, month, date))
    respond_to do |format|
      if @form.save
        @form.update_attribute(:balasan, @form.created_at + 7.day)
        Welcome.send_data(@form, Setting.first.email).deliver
        sms_notivikasi("#{Setting.first.no_hp}", "PMB AMA YPK : Ada Pengdaftar Online #{@form.reg}")
        format.html { redirect_to(@form, :notice => 'Selamat !! data anda telah masuk dalam database kami') }
        format.xml  { render :xml => @form, :status => :created, :location => @form }
      else
        format.html { redirect_to root_url }
        format.xml  { render :xml => @form.errors, :status => :unprocessable_entity }
      end
    end
  end

  def update
    @form.update_attributes(params[:form])
    respond_with(@form)
  end

  def destroy
    @form.destroy
    respond_with(@form)
  end
  
  def sms
    testing("085731775266", "Testing SMS Konfigurasi")
  end

  private
    def set_form
      @form = Form.find(params[:id])
    end
end
