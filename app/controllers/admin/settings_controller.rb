class Admin::SettingsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :set_setting, only: [:show, :edit, :update, :destroy]
  respond_to :html
  include ApplicationHelper

  def index
    @setting = Setting.first
    respond_with(@settings)
  end

  def show
    respond_with(@setting)
  end

  def new
    @setting = Setting.new
    respond_with(@setting)
  end

  def edit
  end

  def create
    @setting = Setting.new(params[:setting])
    if @setting.save
      redirect_to admin_settings_path, :notice => "Successfully created setting."  
    else  
      render :action => 'new'  
    end  
  end

  def update
    @setting.update_attributes(params[:setting])
    redirect_to admin_settings_path, :notice => "Successfully updated setting."
  end

  def destroy
    @setting.destroy
    respond_with(@setting)
  end
  
  def sms
    testing("#{Setting.first.no_hp}", "Test SMS Konfigurasi")
    redirect_to admin_settings_path
  end
  
  def mail
    Welcome.send_reply("#{Setting.first.email}").deliver
    redirect_to admin_settings_path
  end

  private
  def set_setting
    @setting = Setting.find(params[:id])
  end
end
