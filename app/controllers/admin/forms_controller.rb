class Admin::FormsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :set_form, only: [:show, :edit, :update, :destroy]
  respond_to :html

  def index
    @forms = Form.by_year(Setting.first.ta).order("id DESC").where("remove = ?", false)
    respond_to do |format|
      format.html {}
      format.csv { send_data @forms.to_csv }
      format.xls # { send_data @forms.to_csv(col_sep: "\t") }
    end
  end

  def show
    year = @form.created_at.strftime("%Y").to_i
    month = @form.created_at.strftime("%m").to_i
    date = @form.created_at.strftime("%d").to_i
    @form.update_attribute(:gelombang, gelombang(Date.new(year, month, date)))
    @form.update_attribute(:balasan, @form.created_at + 7.day)
    respond_with(@form)
  end

  def new
    @form = Form.new
    @reg = Form.all.size + 1
    respond_with(@form)
  end

  def edit
  end

  def create
    reg = Form.all.size + 1
    @form = Form.new(params[:form])
    @form.reg = "#{"%04d" % reg}/#{Setting.first.ta}"
    @form.ta = "#{Setting.first.ta}"
    @form.save
    redirect_to root_url, :notice => "Successfully Entry ....."
  end

  def update
    @form.update_attributes(params[:form])
    respond_with(@form)
  end

  def destroy
    @form.destroy
    redirect_to admin_forms_path
  end
  
  def reply_email_manual
    @form = Form.find(params[:id])
    Welcome.surat_balasan(@form).deliver
    redirect_to admin_forms_path
  end

  def remove
    @form = Form.find(params[:id])
    @form.update_attribute(:remove, true)
    redirect_to admin_forms_path
  end
  
  def remove_multiple
    Form.where(:id => params[:forms]).update_all(:remove => true)
    redirect_to admin_forms_path
  end
end
  

private
def set_form
  @form = Form.find(params[:id])
end
