class SettingsController < ApplicationController
  before_filter :authenticate_user!
  #load_and_authorize_resource
  before_filter :set_setting, only: [:show, :edit, :update, :destroy]
  respond_to :html

  def index
    @settings = Setting.all
    respond_with(@settings)
  end

  def show
    respond_with(@setting)
  end

  def new
    @setting = Setting.new
    respond_with(@setting)
  end

  def edit
  end

  def create
    @setting = Setting.new(params[:setting])
    #respond_with(@setting)
    if @setting.save
      redirect_to @setting, :notice => "Successfully created setting."  
    else  
      render :action => 'new'  
    end  
  end

  def update
    @setting.update_attributes(params[:setting])
    redirect_to settings_path, :notice => "Successfully updated setting."
  end

  def destroy
    @setting.destroy
    respond_with(@setting)
  end

  private
  def set_setting
    @setting = Setting.find(params[:id])
  end
end
