module ApplicationHelper
  require 'net/http'
  require 'uri'
  
  def display_base_errors resource
    return '' if (resource.errors.empty?) or (resource.errors[:base].empty?)
    messages = resource.errors[:base].map { |msg| content_tag(:p, msg) }.join
    html = <<-HTML
    <div class="alert alert-error alert-block">
      <button type="button" class="close" data-dismiss="alert">&#215;</button>
      #{messages}
    </div>
    HTML
    html.html_safe
  end
  
  def mark_required(object, attribute)  
    "*" if object.class.validators_on(attribute).map(&:class).include? ActiveModel::Validations::PresenceValidator  
  end
  
  def sms_notivikasi(hp, message)
    encoded_url = URI.encode("#{ENV["SMS_API_URL"]}?username=#{Setting.first.username}&password=#{Setting.first.password}&key=#{Setting.first.key}&number=#{hp}&message=#{message}")
    puts encoded_url
    url = URI.parse(encoded_url)
    req = Net::HTTP::Get.new(url.request_uri)
    res = Net::HTTP.start(url.host, url.port) {|http|
      http.request(req)
    }
    notran = res.body.split("|").last
    puts notran
  end
  
  def cek_saldo
    encoded_url = URI.encode("#{ENV["SMS_API_SALDO"]}?username=#{Setting.first.username}&password=#{Setting.first.password}&key=#{Setting.first.key}")
    url = URI.parse(encoded_url)
    req = Net::HTTP::Get.new(url.request_uri)
    res = Net::HTTP.start(url.host, url.port) {|http|
      http.request(req)
    }
    res.body
  end
  
  def saldo
    cek_saldo.split("|").first
  end 

  def expire
    cek_saldo.split("|").last.split(" ").first.to_date
  end

  def testing(number, isi)
    encoded_url = URI.encode("#{ENV["SMS_API_URL"]}?username=#{Setting.first.username}&password=#{Setting.first.password}&key=#{Setting.first.key}&number=#{number}&message=#{isi}")
    url = URI.parse(encoded_url)
    req = Net::HTTP::Get.new(url.request_uri)
    res = Net::HTTP.start(url.host, url.port) {|http|
      http.request(req)
    }
    notran = res.body.split("|").last
    puts notran
  end
  
  def gelombang(a)
    if a >= Setting.first.glk_a.to_date && a <= Setting.first.glk_b.to_date
      "Gelombang Khusus"
    elsif a >= Setting.first.glb1_a.to_date && a <= Setting.first.glb1_b.to_date
      "Gelombang I"
    elsif a >= Setting.first.glb2_a.to_date && a <= Setting.first.glb2_b.to_date
      "Gelombang II"
    else
      "Close"
    end
  end
  
  def indonesia_date(date)
    if date.strftime("%m") == "01"
      "#{date.strftime("%d")} Januari #{date.strftime("%Y")}"
    elsif date.strftime("%m") == "02"
      "#{date.strftime("%d")} Februari #{date.strftime("%Y")}"
    elsif date.strftime("%m") == "03"
      "#{date.strftime("%d")} Maret #{date.strftime("%Y")}"
    elsif date.strftime("%m") == "04"
      "#{date.strftime("%d")} April #{date.strftime("%Y")}"
    elsif date.strftime("%m") == "05"
      "#{date.strftime("%d")} Mei #{date.strftime("%Y")}"
    elsif date.strftime("%m") == "06"
      "#{date.strftime("%d")} Juni #{date.strftime("%Y")}"
    elsif date.strftime("%m") == "07"
      "#{date.strftime("%d")} Juli #{date.strftime("%Y")}"
    elsif date.strftime("%m") == "08"
      "#{date.strftime("%d")} Agustus #{date.strftime("%Y")}"
    elsif date.strftime("%m") == "09"
      "#{date.strftime("%d")} September #{date.strftime("%Y")}"
    elsif date.strftime("%m") == "10"
      "#{date.strftime("%d")} Oktober #{date.strftime("%Y")}"
    elsif date.strftime("%m") == "11"
      "#{date.strftime("%d")} November #{date.strftime("%Y")}"
    elsif date.strftime("%m") == "12"
      "#{date.strftime("%d")} Desember #{date.strftime("%Y")}"
    end
  end
  
  def indonesia_year(date)
    if date.strftime("%m") == "01"
      "#{date.strftime("%d")} Januari"
    elsif date.strftime("%m") == "02"
      "#{date.strftime("%d")} Februari"
    elsif date.strftime("%m") == "03"
      "#{date.strftime("%d")} Maret"
    elsif date.strftime("%m") == "04"
      "#{date.strftime("%d")} April"
    elsif date.strftime("%m") == "05"
      "#{date.strftime("%d")} Mei"
    elsif date.strftime("%m") == "06"
      "#{date.strftime("%d")} Juni"
    elsif date.strftime("%m") == "07"
      "#{date.strftime("%d")} Juli"
    elsif date.strftime("%m") == "08"
      "#{date.strftime("%d")} Agustus"
    elsif date.strftime("%m") == "09"
      "#{date.strftime("%d")} September"
    elsif date.strftime("%m") == "10"
      "#{date.strftime("%d")} Oktober"
    elsif date.strftime("%m") == "11"
      "#{date.strftime("%d")} November"
    elsif date.strftime("%m") == "12"
      "#{date.strftime("%d")} Desember"
    end
  end
  
  def indonesia_bulan(date)
    if date.strftime("%m") == "01"
      "Januari"
    elsif date.strftime("%m") == "02"
      "Februari"
    elsif date.strftime("%m") == "03"
      "Maret"
    elsif date.strftime("%m") == "04"
      "April"
    elsif date.strftime("%m") == "05"
      "Mei"
    elsif date.strftime("%m") == "06"
      "Juni"
    elsif date.strftime("%m") == "07"
      "Juli"
    elsif date.strftime("%m") == "08"
      "Agustus"
    elsif date.strftime("%m") == "09"
      "September"
    elsif date.strftime("%m") == "10"
      "Oktober"
    elsif date.strftime("%m") == "11"
      "November"
    elsif date.strftime("%m") == "12"
      "Desember"
    end
  end
end
