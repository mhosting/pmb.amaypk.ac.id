class AddDuStatusToForm < ActiveRecord::Migration
  def change
    add_column :forms, :du, :integer, :default => 0
    add_column :forms, :status, :integer, :default => 0
  end
end
