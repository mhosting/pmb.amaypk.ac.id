class AddColumnSmsToSetting < ActiveRecord::Migration
  def change
    add_column :settings, :username, :string
    add_column :settings, :password, :string
    add_column :settings, :key, :string
  end
end
