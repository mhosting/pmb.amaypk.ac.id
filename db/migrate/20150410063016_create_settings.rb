class CreateSettings < ActiveRecord::Migration
  def change
    create_table :settings do |t|
      t.string :ta

      t.timestamps
    end
  end
end
