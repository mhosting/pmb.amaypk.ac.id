class CreateForms < ActiveRecord::Migration
  def change
    create_table :forms do |t|
      t.string :reg
      t.string :name
      t.string :ttl
      t.string :tlp_hp
      t.string :email
      t.string :jk
      t.string :tb
      t.string :bb
      t.string :agama
      t.string :asal_sekolah
      t.string :jurusan
      t.text :alamat_sekolah
      t.string :email_sekolah
      t.string :web_sekolah
      t.string :nama_gbk
      t.string :tlp_bk
      t.string :nama_ortu
      t.string :telp_ortu
      t.text :alamat_ortu
      t.string :job_ortu
      t.string :penghasilan_ortu
      t.string :prodi

      t.timestamps
    end
  end
end
