class AddGelombangPendaftaranToSetting < ActiveRecord::Migration
  def change
    add_column :settings, :glk_a, :string
    add_column :settings, :glk_b, :string
    add_column :settings, :glb1_a, :string
    add_column :settings, :glb1_b, :string
    add_column :settings, :glb2_a, :string
    add_column :settings, :glb2_b, :string
  end
end
