class AddBuyToSetting < ActiveRecord::Migration
  def change
    add_column :settings, :du, :string
    add_column :settings, :jas_seragam, :string
    add_column :settings, :ku, :string
    add_column :settings, :kp, :string
    add_column :settings, :khusus_a, :string
    add_column :settings, :khusus_b, :string
    add_column :settings, :gel_1_a, :string
    add_column :settings, :gel_1_b, :string
    add_column :settings, :gel_2_a, :string
    add_column :settings, :gel_2_b, :string
  end
end
