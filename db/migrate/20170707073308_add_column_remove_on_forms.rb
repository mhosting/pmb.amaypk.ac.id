class AddColumnRemoveOnForms < ActiveRecord::Migration
  def change
    add_column :forms, :remove, :boolean, :default => false
  end
end
