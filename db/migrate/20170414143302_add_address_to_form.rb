class AddAddressToForm < ActiveRecord::Migration
  def change
    #alamat pribadi
    add_column :forms, :alamat_pribadi_dusun, :string
    add_column :forms, :alamat_pribadi_rt, :string
    add_column :forms, :alamat_pribadi_kelurahan_desa, :string
    add_column :forms, :alamat_pribadi_kecamatan, :string
    add_column :forms, :alamat_pribadi_kabupaten, :string
    add_column :forms, :alamat_pribadi_propinsi, :string
    #alamat sekolah
    add_column :forms, :alamat_sekolah_dusun, :string
    add_column :forms, :alamat_sekolah_rt, :string
    add_column :forms, :alamat_sekolah_kelurahan_desa, :string
    add_column :forms, :alamat_sekolah_kecamatan, :string
    add_column :forms, :alamat_sekolah_kabupaten, :string
    add_column :forms, :alamat_sekolah_propinsi, :string
    #alamat ortu
    add_column :forms, :alamat_ortu_dusun, :string
    add_column :forms, :alamat_ortu_rt, :string
    add_column :forms, :alamat_ortu_kelurahan_desa, :string
    add_column :forms, :alamat_ortu_kecamatan, :string
    add_column :forms, :alamat_ortu_kabupaten, :string
    add_column :forms, :alamat_ortu_propinsi, :string
  end
end
