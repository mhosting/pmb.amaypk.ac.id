# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20150423135716) do

  create_table "forms", :force => true do |t|
    t.string   "reg"
    t.string   "name"
    t.string   "ttl"
    t.string   "tlp_hp"
    t.string   "email"
    t.string   "jk"
    t.string   "tb"
    t.string   "bb"
    t.string   "agama"
    t.string   "asal_sekolah"
    t.string   "jurusan"
    t.text     "alamat_sekolah"
    t.string   "email_sekolah"
    t.string   "web_sekolah"
    t.string   "nama_gbk"
    t.string   "tlp_bk"
    t.string   "nama_ortu"
    t.string   "telp_ortu"
    t.text     "alamat_ortu"
    t.string   "job_ortu"
    t.string   "penghasilan_ortu"
    t.string   "prodi"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.text     "alamat"
    t.text     "ta"
  end

  create_table "settings", :force => true do |t|
    t.string   "ta"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "no_hp"
    t.string   "email"
  end

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0,  :null => false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

end
